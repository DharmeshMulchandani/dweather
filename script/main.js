const body = document.querySelector("body");
const place = document.querySelector("#weather-place");
const temp = document.querySelector("#weather-temp");
const weatherType = document.querySelector("#weather-type");
const currenticon = document.querySelector("#weather-icon");

const feelsLike = document.querySelector("#feels-like");
const humidity = document.querySelector("#humidity");
const wind = document.querySelector("#wind");
const sunriseTime = document.querySelector("#sunrise");
const sunsetTime = document.querySelector("#sunset");
const maxTemp = document.querySelector("#maxtemp");
const minTemp = document.querySelector("#mintemp");

const weatherCards = document.querySelector("#weather-card");

const searchForm = document.querySelector("#search-form");
const searchInput = document.querySelector("#search");

let api = `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/mumbai?unitGroup=us&key=YOUR APIKEY&contentType=json&iconSet=icons1`;
let parsedResponse;
let weatherCondition;
let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const handleEvents = () => {
    document.addEventListener("DOMContentLoaded", fetchWeather);
    searchForm.addEventListener("submit", search);
}


const fetchWeather = () => {
    fetch(api).then(function(response) {
        if(response.status === 200) {
            return response.json();
        }
        throw new Error(`Error: ${response.status} MESSAGE: ${response.message}`);
    })
    .then(function(weather){
        parsedResponse = weather;
        showWeather();
    })
    .catch(function(err){
        console.warn(err);
    })
}

const showWeather = () => {
    place.innerText = parsedResponse.address;
    temp.innerText = fahrenheitToCelsius(parsedResponse.currentConditions.temp) + "°";
    weatherCondition = weatherType.innerText = parsedResponse.currentConditions.conditions;
    
    let icon = parsedResponse.currentConditions.icon;
    currenticon.src = `./images/${icon}.svg`;

    feelsLike.innerHTML = `Feels like ${fahrenheitToCelsius(parsedResponse.currentConditions.feelslike)}°`;
    humidity.innerHTML = `Humidity ${parsedResponse.currentConditions.humidity}`;
    wind.innerHTML = `Wind ${parsedResponse.currentConditions.windspeed}`;
    sunriseTime.innerHTML = `Sunrise ${parsedResponse.currentConditions.sunrise} AM`;
    sunsetTime.innerHTML = `Sunset ${parsedResponse.currentConditions.sunset} PM`;
    minTemp.innerHTML = `Min temp ${fahrenheitToCelsius(parsedResponse.days[0].tempmin)}°`;
    maxTemp.innerHTML = `Max temp ${fahrenheitToCelsius(parsedResponse.days[0].tempmax)}°`;

    for (let i = 1; i < 7; i++) {
        let date = parsedResponse.days[i].datetimeEpoch;
        let epocConvertedDate = new Date(date * 1000);
        let day = days[epocConvertedDate.getDay()];
        let dayIcon = parsedResponse.days[i].icon;
        let dayTemp = Math.round(parsedResponse.days[i].temp);
    
        createDayWeatherCard(day, dayIcon, dayTemp);
    }
    switchTheme();
}

const createDayWeatherCard = (day, dayIcon, dayTemp) => {
    const weatherCard = document.createElement("div");
    weatherCard.className = "weather-card d-flex flex-center flex-direction-column";
  
    const weatherCardText = document.createElement("h4");
    weatherCardText.className = "weather-card__text";
    weatherCardText.innerText = `${day}`;
    weatherCard.appendChild(weatherCardText);
  
    const img = document.createElement("img");
    img.className = "img-responsive";
    img.src = `./images/${dayIcon}.svg`;
    weatherCard.appendChild(img);
  
    const weatherCardTemp = document.createElement("h4");
    weatherCardTemp.className = "weather-card__text";
    weatherCardTemp.innerText = `${fahrenheitToCelsius(dayTemp)}°`;
    weatherCard.appendChild(weatherCardTemp);
  
    weatherCards.appendChild(weatherCard);   
}

const switchTheme = () => {
    body.className = "";
    let condition = parsedResponse.currentConditions.conditions;
    console.log(parsedResponse);

    if (condition.indexOf("Partially cloudy") !== -1) {
        body.className = "rainy-morning";
    } else if (condition.indexOf("Clear") !== -1) {
        body.className = "clear-sky-morning";
    } else if (condition.indexOf("Overcast") !== -1) {
        body.className = "snow-night";
    }
    $(body).fadeOut(1000).fadeIn();
} 

const search = (evt) => {
    evt.preventDefault();
    if (searchInput.value == "") {
        alert("Please enter a location");
    } else {
        let location = searchInput.value;
        console.log(location);
        api = `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/${location}?unitGroup=us&key=YOUR APIKEY&contentType=json&iconSet=icons1`;

        weatherCards.innerHTML = " ";
        fetchWeather();
    }
    searchInput.value = "";
}

const fahrenheitToCelsius = f => Math.round((f - 32) / 1.8);

handleEvents();